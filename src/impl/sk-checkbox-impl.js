

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';
import { SkRequired } from '../../../sk-form-validator/src/validators/sk-required.js';


export var CHK_FORWARDED_ATTRS = {
    'disabled': 'disabled', 'checked': 'checked', 'required': 'required'
};


export class SkCheckboxImpl extends SkComponentImpl {

    get suffix() {
        return 'checkbox';
    }

    get input() {
        return this.comp.el.querySelector('input');
    }

    onChange(event) {
        this.comp.setAttribute('value', event.target.value);
        if (event.target.checked) {
            this.comp.setAttribute('checked', 'checked');
        } else {
            this.comp.removeAttribute('checked');
        }
        let result = this.validateWithAttrs();
        if (typeof result === 'string') {
            this.input.setCustomValidity(result);
            this.showInvalid();
        }
        this.comp.dispatchEvent(new CustomEvent('change', { target: this.comp, detail: { value: event.target.value, checked: event.target.checked }, bubbles: true, composed: true }));
        this.comp.dispatchEvent(new CustomEvent('skchange', { target: this.comp, detail: { value: event.target.value, checked: event.target.checked }, bubbles: true, composed: true }));
    }

    onFocus(event) {
        this.removeUntouched();
    }

    removeUntouched() {
        if (this.input && this.input.classList.contains('untouched')) {
            this.input.classList.remove('untouched');
        }
    }

    setUntouched() {
        if (this.input) {
            this.input.classList.add('untouched');
        }
    }

    bindEvents() {
        super.bindEvents();
        this.input.onchange = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onChange(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.input.onfocus = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onFocus(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
    }

    unbindEvents() {
        super.unbindEvents();
        this.input.onchange = null;
        this.input.oninput = null;
    }

    restoreState(state) {
        super.restoreState(state);
        this.comp.clearElCache();
        this.forwardAttributes();
        this.bindEvents();
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState();
        this.setUntouched();
    }

    forwardAttributes() {
        if (this.input) {
            for (let attrName of Object.keys(CHK_FORWARDED_ATTRS)) {
                let value = this.comp.getAttribute(attrName);
                if (value) {
                    this.input.setAttribute(attrName, value);
                } else {
                    if (this.comp.hasAttribute(attrName)) {
                        this.input.setAttribute(attrName, '');
                    }
                }
            }
        }
    }

    check() {
        this.input.checked = 'checked';
    }

    uncheck() {
        this.input.checked = false;
    }

    disable() {

    }

    enable() {

    }

    get skValidators() {
        if (! this._skValidators) {
            this._skValidators = {
                'sk-required': new SkRequired(),
            };
        }
        return this._skValidators;
    }

    showInvalid() {
        super.showInvalid();
        this.input.classList.add('form-invalid');
    }

    showValid() {
        super.showValid();
        this.input.classList.remove('form-invalid');
    }

    renderValidation(validity) {
        this.comp.inputEl.setCustomValidity(validity);
        super.renderValidation();
    }

    flushValidation() {
        super.flushValidation();
        this.comp.inputEl.setCustomValidity('');
    }

    focus() {
        setTimeout(() => {
            this.comp.inputEl.focus();
        }, 0);
    }
}