
import { SkElement } from '../../sk-core/src/sk-element.js';

import { CHK_FORWARDED_ATTRS } from './impl/sk-checkbox-impl.js';

export class SkCheckbox extends SkElement {

    get cnSuffix() {
        return 'checkbox';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get subEls() {
        return [ 'inputEl' ];
    }

    get inputEl() {
        if (! this._inputEl && this.el) {
            this._inputEl = this.el.querySelector('input');
        }
        return this._inputEl;
    }

    set inputEl(el) {
        this._inputEl = el;
    }

    get validationMessage() {
        return this.inputEl && this.inputEl.validationMessage
            && this.inputEl.validationMessage !== 'undefined' ? this.inputEl.validationMessage : this.locale.tr('Field value is invalid');
    }

    set validationMessage(validationMessage) {
        this._validationMessage = validationMessage;
    }

    get validity() {
        return this.inputEl ?  this.inputEl.validity : false;
    }

    set checked(checked) {
        this.setAttribute('checked', checked);
    }

    get checked() {
        return this.getAttribute('checked');
    }

    set value(value) {
        if (value === null) {
            this.removeAttribute('value');
        } else {
            this.setAttribute('value', value);
        }
    }

    get value() {
        if (this.getAttribute('checked')) {
            return this.getAttribute('value');
        } else {
            return null;
        }
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (! this.inputEl) {
            return false;
        }
        if (CHK_FORWARDED_ATTRS[name] && oldValue !== newValue) {
            if (newValue !== null && newValue !== undefined) {
                this.inputEl.setAttribute(name, newValue);
            } else {
                this.inputEl.removeAttribute(name);
            }
        }
        if (name === 'checked' && ! this.inact) {
            if (newValue) {
                this.impl.check();
            } else {
                this.impl.uncheck();
            }
        }
        if (name === 'disabled') {
            if (newValue) {
                this.impl.disable();
            } else {
                this.impl.enable();
            }
        }
        if (name === 'form-invalid') {
            if (newValue !== null) {
                this.impl.showInvalid();
            } else {
                this.impl.showValid();
            }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }

    static get observedAttributes() {
        return ['form-invalid', ...Object.keys(CHK_FORWARDED_ATTRS)];
    }

}
